﻿namespace WindowsFormsApplication11
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.applyFilterButton = new System.Windows.Forms.Button();
            this.filterTextBox = new System.Windows.Forms.TextBox();
            this.grupeComboBox = new System.Windows.Forms.ComboBox();
            this.addMockData = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.printButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.nameDataGridView = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // applyFilterButton
            // 
            this.applyFilterButton.Location = new System.Drawing.Point(561, 12);
            this.applyFilterButton.Name = "applyFilterButton";
            this.applyFilterButton.Size = new System.Drawing.Size(63, 23);
            this.applyFilterButton.TabIndex = 0;
            this.applyFilterButton.Text = "Filtreaza";
            this.applyFilterButton.UseVisualStyleBackColor = true;
            this.applyFilterButton.Visible = false;
            this.applyFilterButton.Click += new System.EventHandler(this.applyFilterButton_Click);
            // 
            // filterTextBox
            // 
            this.filterTextBox.Location = new System.Drawing.Point(12, 12);
            this.filterTextBox.Name = "filterTextBox";
            this.filterTextBox.Size = new System.Drawing.Size(120, 20);
            this.filterTextBox.TabIndex = 2;
            this.filterTextBox.TextChanged += new System.EventHandler(this.filterTextBox_TextChanged);
            // 
            // grupeComboBox
            // 
            this.grupeComboBox.FormattingEnabled = true;
            this.grupeComboBox.Location = new System.Drawing.Point(139, 12);
            this.grupeComboBox.Name = "grupeComboBox";
            this.grupeComboBox.Size = new System.Drawing.Size(72, 21);
            this.grupeComboBox.TabIndex = 4;
            this.grupeComboBox.SelectedIndexChanged += new System.EventHandler(this.grupeComboBox_SelectedIndexChanged);
            // 
            // addMockData
            // 
            this.addMockData.Location = new System.Drawing.Point(630, 12);
            this.addMockData.Name = "addMockData";
            this.addMockData.Size = new System.Drawing.Size(75, 23);
            this.addMockData.TabIndex = 5;
            this.addMockData.Text = "Add data";
            this.addMockData.UseVisualStyleBackColor = true;
            this.addMockData.Visible = false;
            this.addMockData.Click += new System.EventHandler(this.addMockData_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Menu;
            this.panel1.Controls.Add(this.printButton);
            this.panel1.Controls.Add(this.filterTextBox);
            this.panel1.Controls.Add(this.addMockData);
            this.panel1.Controls.Add(this.applyFilterButton);
            this.panel1.Controls.Add(this.grupeComboBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(716, 45);
            this.panel1.TabIndex = 6;
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(229, 13);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(75, 23);
            this.printButton.TabIndex = 6;
            this.printButton.Text = "Print";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.nameDataGridView);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 45);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(716, 527);
            this.panel2.TabIndex = 7;
            // 
            // nameDataGridView
            // 
            this.nameDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.nameDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.nameDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nameDataGridView.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.nameDataGridView.Location = new System.Drawing.Point(0, 0);
            this.nameDataGridView.Name = "nameDataGridView";
            this.nameDataGridView.ReadOnly = true;
            this.nameDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.nameDataGridView.Size = new System.Drawing.Size(716, 527);
            this.nameDataGridView.TabIndex = 4;
            this.nameDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.nameDataGridView_CellContentClick_1);
            this.nameDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.nameDataGridView_CellFormatting_1);
            this.nameDataGridView.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.nameDataGridView_ColumnHeaderMouseDoubleClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 572);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nameDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button applyFilterButton;
        private System.Windows.Forms.TextBox filterTextBox;
        private System.Windows.Forms.ComboBox grupeComboBox;
        private System.Windows.Forms.Button addMockData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView nameDataGridView;
    }
}

