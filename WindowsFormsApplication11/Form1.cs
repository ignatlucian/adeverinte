﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication11.DataProvider;

namespace WindowsFormsApplication11
{
    public partial class Form1 : Form
    {

        List<Person> persons = new List<Person>();
        public Form1()
        {
            InitializeComponent();
        }

        private void applyFilterButton_Click(object sender, EventArgs e)
        {
            ApplyFilter();
        }



        private void filterTextBox_TextChanged(object sender, EventArgs e)
        {
            ApplyFilter();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataProvider.UserProvider.LoadDataBuffer(
                @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source= test.accdb");           var grupeList = new List<DataProvider.Grupe>();
            grupeList.Add(new DataProvider.Grupe { IdGrupa = 0, Grupa = 0 });
            grupeList.AddRange(DataProvider.UserProvider.grupeList);
            grupeComboBox.DataSource = grupeList;
            grupeComboBox.DisplayMember = "Grupa";
            grupeComboBox.SelectedItem = "idGrupe";

            LoadOptions(Paths.optionsPath);


            ApplyFilter();

         }

        private void LoadOptions(string optionsPath)
        {
            filterTextBox.Text = Properties.Settings.Default.filterTextBox_Text;
            grupeComboBox.SelectedIndex = Properties.Settings.Default.grupeComboBox_SelectedIndex;
        }

        private void addMockData_Click(object sender, EventArgs e)
        {
            DataProvider.MockDataProvider.AddMockData("studenti.txt",Paths.connectionString);
            DataProvider.UserProvider.LoadDataBuffer(Paths.connectionString);
            ApplyFilter();
        }

        private void grupeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyFilter();
        }

        private void ApplyFilter()
        {
            persons = DataProvider.UserProvider.GetPersonsByNameAndGrupa(filterTextBox.Text.ToUpper(),
                Convert.ToInt32(((Grupe)grupeComboBox.SelectedItem).IdGrupa));    
                nameDataGridView.DataSource = persons;
        }

        private void nameDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
         }

        private void nameDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void nameDataGridView_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void nameDataGridView_CellFormatting_1(object sender, DataGridViewCellFormattingEventArgs e)
        {
            for (int i = 0; i < nameDataGridView.Rows.Count; i++)
            {
                nameDataGridView.Rows[i].DefaultCellStyle.BackColor =
                    (i % 2 == 0) ? Color.LightBlue : Color.LightCyan;
            }

        }

        private void printButton_Click(object sender, EventArgs e)
        {
            List<Person> personListToPrinter = PersonListToPrinter();
            Business_Logic.Printing.PrintPersons(personListToPrinter);
        }

        private List<Person> PersonListToPrinter()
        {
            List<Person> personListToPrinter = new List<Person>();

            foreach (DataGridViewRow person in nameDataGridView.SelectedRows)
            {
                personListToPrinter.Add(
                    new Person
                    {
                        IdCode = Convert.ToInt32(person.Cells["IdCode"].Value),
                        Name = person.Cells["Name"].Value.ToString(),
                        Surname = person.Cells["Surname"].Value.ToString(),
                        Grupa = Convert.ToInt32(person.Cells["Grupa"].Value),

                    }
                );
            }

            return personListToPrinter;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.filterTextBox_Text = filterTextBox.Text;
            Properties.Settings.Default.grupeComboBox_SelectedIndex = grupeComboBox.SelectedIndex;
            Properties.Settings.Default.Save();
        }

        private void nameDataGridView_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                persons = persons.OrderBy(x => x.Grupa).ToList();
                nameDataGridView.DataSource = persons;
            }
        }

    }
}
