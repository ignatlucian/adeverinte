﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication11.DataProvider
{
    class MockDataProvider
    {
        internal static void AddMockData(string inputFileName, string connectionString)
        {
            List<string> fullNames = GetFullNamesFromFile(inputFileName);

            Random grupaId = new Random();

            DeleteByPartOfName(connectionString, "");
            foreach (var name in fullNames)
            {
                Insert(connectionString, UserProvider.GetNameFromFullName(name),
                    UserProvider.GetSurnameFromFullName(name),grupaId.Next(1,4));
                ;
            }

        }

        private static void DeleteByPartOfName(string connectionString, string text)
        {
            OleDbConnection connection = new OleDbConnection(connectionString);
            OleDbCommand command = new OleDbCommand();
            command.CommandText = @"delete *  from studenti where name LIKE '%" + text +"%' ";

            command.Connection = connection;
            connection.Open();
            command.ExecuteScalar();
            connection.Close();

        }

        private static List<string> GetFullNamesFromFile(string inputFileName)
        {
            var buffer = System.IO.File.ReadAllLines(inputFileName);
            return buffer.ToList();
        }

        private static void Insert(string connectionString, string name, string surname, int grupaId)
        {
            OleDbConnection connection = new OleDbConnection(connectionString);
            OleDbCommand command = new OleDbCommand();
            command.CommandText = @"insert into Studenti    ([Name],[Surname],[idGrupa]) 
                                                    values  (@name,@surname,@grupaId)";
            command.Parameters.AddWithValue("@name", name);
            command.Parameters.AddWithValue("@surnamme", surname);
            command.Parameters.AddWithValue("@grupaId", grupaId);

            command.Connection = connection;
            connection.Open();
            command.ExecuteScalar();
            connection.Close();
        }
    }
}
