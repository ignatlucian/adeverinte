﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication11.DataProvider
{
    class Grupe
    {
        int idGrupa;
        int grupa;

        public int IdGrupa
        {
            get
            {
                return idGrupa;
            }

            set
            {
                idGrupa = value;
            }
        }

        public int Grupa
        {
            get
            {
                return grupa;
            }

            set
            {
                grupa = value;
            }
        }
    }
}
