﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication11.Helpers
{
    static class StringHelper
    {
        public static string EraseFirstWord(string inputString)
        {
            var splitedString = inputString.Split(' ');

            string outputString = splitedString[1];
            for (int i = 2; i < splitedString.Count(); i++)
            {
                outputString += " " + splitedString[i];
            }
            return outputString;
        }
    }
}
